# frozen_string_literal: true

class Student
  attr_accessor :first_name, :last_name, :email
  attr_reader :username
  @first_name
  @last_name
  @email
  @username
  @password

  def initialize(first_name, last_name, username, email, password)
    @first_name = first_name
    @last_name = last_name
    @email = email
    @username = username
    @password = password
  end

  def to_s
    "#{@first_name} #{@last_name} #{@email} #{@username}"
  end
end

me = Student.new('Jaime', 'Martinez', 'jm1@email.com', 'jm1', 'password1')

puts me
