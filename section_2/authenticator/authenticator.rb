# frozen_string_literal: true

users = [
  { username: 'jaime', password: 'password1', attempts: 0 },
  { username: 'jack', password: 'password2', attempts: 0  },
  { username: 'arya', password: 'password3', attempts: 0  },
  { username: 'jonsnow', password: 'password4', attempts: 0 },
  { username: 'heisenberg', password: 'password5', attempts: 0 }
]
def sep
  50.times { print '-' }
  puts
end

def run(users)
  username = prompt('username')
  password = prompt('password')

  sep

  user = users.find { |u| u[:username] == username } #   return user
  if user.nil?
    puts "username '#{username}' doesn't exist! stop hacking meeee!"
    return nil
  end
  attempts = 0
  while attempts < 3
    if password.empty?
      password = prompt('password')
      sep
    end
    if user[:password] == password
      puts 'password matches!!!!!'
      return user
    else
      password = ''
      puts 'uh oh!'
      attempts += 1
     end
    puts "Incorrect password, #{3 - attempts} attempts remaining..."
end
  puts 'incorrect password, bye!'
  nil
end

def prompt(key)
  puts "Please enter your #{key}"
  gets.chomp
end

puts 'Welcome to the authenticator'
sep
puts 'Comparing username and password!'
puts 'will get user object or fail'
sep

user = run(users)

puts "found user! #{user}" unless user.nil?
