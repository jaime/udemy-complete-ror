# frozen_string_literal: true

require_relative 'Crud'

# Student class
class Student
  include Crud
  attr_accessor :first_name, :last_name, :email, :username, :password

  def initialize(first_name, last_name, username, email, password)
    @first_name = first_name
    @last_name = last_name
    @email = email
    @username = username
    @password = password
  end

  def to_s
    "#{@first_name} #{@last_name} #{@email} #{@username}"
  end
end

jaime = Student.new('jaime', 'martinez', 'jm1', 'jm1@email.com', 'pass1234')
pass = jaime.create_hash_digest(jaime.password)

puts pass
