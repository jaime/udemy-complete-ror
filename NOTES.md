# Notes

Document gotchas and some Ruby magic I've found so far

## Ruby

### Comparison with == 

My understanding is that `==` is a method that all classes implement. So when you are comparing strings the `String#==` will be called. I ran into the following problem while working on [bcrypt](./bcrypt/crud.rb#26)

```rb
def verify_hash_digest(password)
  BCrypt::Password.new(password)
end
puts verify_hash_digest('$2a$12$l2DReXa7Bsw6U.1rH31fv.qBtiVwGRdTqk/R7NXNqrtlvYiJmNtYi') == 'password1'
# true
puts 'password1' == verify_hash_digest('$2a$12$l2DReXa7Bsw6U.1rH31fv.qBtiVwGRdTqk/R7NXNqrtlvYiJmNtYi') 
# false
```

After getting help from [krasio](https://gitlab.com/krasio) (thank you!) he noted that the `bcrypt` library implements a class named Password like this

```rb
class Password < String
```

This means that `Password` is a child of `String` so we can safely assume the following

*All passwords are strings but not all strings are passwords*

That means that comparing `BCrypt::Password#==` to a `String` will be true. However, comparing a `String#==` to a `Password` will fail. This explains why the second statement in the line above is false!


## Rails

### Scaffolding

Is a great way to get started with Ruby on Rails very quickly. It generates the models, including the DB migrations, the controller for all articles, the views for each and the routes. This provides a full `CRUD` system for an `Entity`! I can see now why people love this approach. However, there's a lot of magic happening behind the scenes, so for people like me that have some experience writing web services from scratch, this magic is a bit too much and you delegate all control to Rails.

```shell
rails generate scaffold Entity attr:type another_attr:type
```
