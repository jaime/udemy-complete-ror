# frozen_string_literal: true

require_relative 'crud'

## to not use relative this can be done
# $LOAD_PATH << "."
# require 'crud'

users = [
  { username: 'jaime', password: 'password1' },
  { username: 'jack', password: 'password2' },
  { username: 'arya', password: 'password3' },
  { username: 'jon', password: 'password4' },
  { username: 'bran', password: 'password5' }
]

Crud.create_secure_users(users)

puts users
