# frozen_string_literal: true

hash = { a: 1, b: 'hi', c: 'adios', d: 4 }

puts hash

hash.each { |key, value| puts "key: #{key} val:#{value}" }

hash.select { |_k, v| puts v.is_a?(String) }

another = hash.select { |k, v| hash.delete(k) if v.is_a?(String) }
p another
puts
p hash
