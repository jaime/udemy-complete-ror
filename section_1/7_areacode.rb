# frozen_string_literal: true

dial_book = {
  'newyork' => '212',
  'newbrunswick' => '732',
  'edison' => '908',
  'plainsboro' => '609',
  'sanfrancisco' => '301',
  'miami' => '305',
  'paloalto' => '650',
  'evanston' => '847',
  'orlando' => '407',
  'lancaster' => '717'
}

def get_city_names(dial_book)
  cities = []
  dial_book.each { |key, _| cities.push(key) }
  cities
end

def get_area_code(dial_book, city)
  dial_book[city]
end

def main(dial_book)
  yn = ''
  while yn.upcase != 'N'
    puts 'Do you want to lookup an area code based on a city name? (Y/N)'
    yn = gets.chomp
    if yn.upcase == 'Y'
      puts 'Which city do you want the area code for?'
      puts get_city_names(dial_book)
      puts 'Enter your selection'
      city = gets.chomp
      area_code = get_area_code(dial_book, city)
      if area_code.nil?
        p "Seems like #{city} does not exist in our database!"
      else
        p "The area code for #{city} is #{area_code}"
      end
    elsif yn.upcase == 'N'
      puts 'Okay, bye!'
    else
      puts "Invalid option: '#{yn}'. Please type Y or N"
    end
  end
end
main(dial_book)
