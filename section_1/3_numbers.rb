# frozen_string_literal: true

# puts 1+2

# 10.times { puts rand(100)}
def sep
  40.times { print '--' }
  puts
end

puts 'Simple calculator'
sep
sep
puts 'What do you want to do?'
sep
puts "1. Add\n2. Subtract\n3. Multiply\n4. Divide\n5. Modulo"
choice = gets.chomp
sep
puts "\nEnter num_1 number:"
num_1 = gets.chomp
sep
puts "\nEnter num_2 number:"
num_2 = gets.chomp
sep
sep

def add(num_1, num_2)
  num_1.to_f + num_2.to_f
end

def subtract(num_1, num_2)
  num_1.to_f - num_2.to_f
end

def multiply(num_1, num_2)
  num_1.to_f * num_2.to_f
end

def divide(num_1, num_2)
  num_1.to_f / num_2.to_f
end

def modulo(num_1, num_2)
  num_1.to_f % num_2.to_f
end

def action(num_1, num_2, choice)
  if choice == '1'
    puts "#{num_1} + #{num_2} = #{add(num_1, num_2)}"
  elsif choice == '2'
    puts "#{num_1} - #{num_2} = #{substract(num_1, num_2)}"
  elsif choice == '3'
    puts "#{num_1} * #{num_2} = #{multiply(num_1, num_2)}"
  elsif choice == '4'
    puts "#{num_1} / #{num_2} = #{divide(num_1, num_2)}"
  elsif choice == '5'
    puts "#{num_1} % #{num_2} = #{modulo(num_1, num_2)}"
  else
    puts 'invalid option, bye.'
  end
end

action(num_1, num_2, choice)
