# frozen_string_literal: true

a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
p a
p a.last
p a.first

# x = 1..100
# puts x.class
# p x
# p x.to_a
# p x.to_a.shuffle
# puts x.class
# z = x.to_a.shuffle!
# p z

# alphabet = 'a'..'z'
# p alphabet.to_a

p a.unshift('a new element')
p a.append('a new element')
p a.uniq
p a
p 'whaaat'
p a.uniq!
p a.empty?
b = []
p b.empty?

p a.include?(22)
p a.include?(2)
p a.shift
p a
